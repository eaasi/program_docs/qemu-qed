# [qemu-qed](https://eaasi.gitlab.io/program_docs/qemu-qed/)

The missing manual for QEMU for digital preservation

## How do I contribute?

You are welcome to edit the codebase yourself, or just supply the information and ask it to be added to the site.

#### Edit codebase

To contribute to this project directly (and more quickly), clone this repository and create a new branch (`git checkout -b your-branch-name`) and add or modify any of the pages or posts on the site! 

Everything you see on the site can be found in the `content` folder. From there you can add to the Glossary, Installing QEMU, or Resources pages by directly editing their corresponding `.md` Markdown file.

To add a knowledge base article to "Using QEMU" or a particular recommended OS recipe, add a *new* Markdown file with appropriate title and front matter to the corresponding folder (`usage` for Using QEMU, "recipes" for recipes). Feel free to use any of the existing Markdown files in those folders as a template.

When you're done editing, [submit a merge request](https://gitlab.com/eaasi/program_docs/qemu-qed/merge_requests) and the maintainers will review and integrate your additions.

#### Make a request

If you are having trouble with coding it yourself or with GitLab, feel free to [submit an issue](https://gitlab.com/eaasi/program_docs/qemu-qed/issues) with the content you would like to see added to the site!

#### General help

If you want to help but don't have anything new to add, you can help us by testing out the scripts available, by refining or clarifying the existing documentation, or [creating an issue](https://gitlab.com/eaasi/program_docs/qemu-qed/issues) for anything that sounds confusing to you.
