+++
title = "Live-Swap Floppy Disks or CD-ROMs"
description = "How to switch or eject removable media like floppy disks or CD-ROMs from a running QEMU session"
+++

Disk images representing removable media such as floppy disks or optical/CD-ROMs do not *need* to be specified at the command line when first booting QEMU in order to view/run them in emulation. They can be inserted, swapped, or ejected from an emulated machine "on-the-fly" just as you would with physical media and a real computer.

To do so, however, requires use of the QEMU Monitor. Please consult the [Using the QEMU Monitor](../qemu_monitor) page for details on how to open the QEMU Monitor, then the sub-section on [Swapping Removable Disks](../qemu_monitor#swapping-removable-disks) for specific instructions to switch/change/eject floppy disk and optical media images.