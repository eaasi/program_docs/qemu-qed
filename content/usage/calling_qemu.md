+++
title = "Calling QEMU from the Command Line"
description = "How to start using QEMU by picking a CPU architecture"
weight = 1
+++

QEMU is technically a bundle of applications that emulate several different computing platforms or [architectures](/glossary#architecture). The first step in using QEMU is to decide which architecture is to be emulated, which will determine the command used to invoke QEMU from a command line terminal.

This is not as complicated as it sounds. The best method is to figure out the target operating system you are trying to emulate (e.g. Windows 95) and work backwards. Generally, only a few platforms/commands are currently relevant for legacy material from the PC era:

    $ qemu-system-i386

  Emulates the [Intel x86 32-bit architecture](https://en.wikipedia.org/wiki/I386). (aka "i386", "x86", "x86_32", or "IA-32") Relevant for IBM/PC/MS-DOS, all Windows systems up until 64-bit Windows XP, and many Linux, BSD, or other open source operating systems from, roughly, the late 1980s through the mid-2000s.

    $ qemu-system-x86_64
  
  Emulates [Intel x86 64-bit](https://en.wikipedia.org/wiki/X86-64) architecture. (aka "x86_64", "x64") Can be used to run more recent PC operating systems like any Windows system newer than Windows XP 64-bit, or again many Linux, BSD, or other open source systems.

    $ qemu-system-ppc
    
  Emulates the [PowerPC](https://en.wikipedia.org/wiki/PowerPC) architecture. Mostly relevant to emulate certain legacy MacOS versions (9.2.2, early OSX), some Linux systems.

All platforms currently supported by QEMU are documented [here](https://wiki.qemu.org/Documentation/Platforms), but again these will largely be used in edge cases for more specialized hardware.

-----

**QEMU QED examples will use qemu-system-i386 in examples, except in specific [recipes](/recipes/) where required.**
