+++
title = "Increase Disk Image Size"
description = "Use qemu-img to increase the size of a disk image"
+++

```bash
qemu-img resize Win2k.img +20G
```
<kbd>qemu-img</kbd>  
  calls the QEMU program to work with disk images
  
<kbd>resize</kbd>  
  instructs the program to increase or decrease the size of an existing disk image
  
<kbd>Win2k.img</kbd>  
  defines the path and file name of the existing disk image
  
<kbd>+20G</kbd>  
  defines the size of the increase 
  
To decrease the size of a disk image, be sure to consult the command's [documentation](https://linux.die.net/man/1/qemu-img) because this may corrupt the disk image if not careful!
