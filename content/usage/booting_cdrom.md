+++
title = "Booting from a CD-ROM File"
description = "Boot an emulated machine from a CD-ROM file"
+++
```bash
qemu-system-i386 -cdrom filename.iso
```

<kbd>qemu-system-i386</kbd>  
    calls the program and emulates an i386-architecture machine
    
<kbd>-cdrom</kbd>  
    tells the program you are sending it a CD-ROM type of file  
    
<kbd>filename.iso</kbd>  
    path and name of the input file 
