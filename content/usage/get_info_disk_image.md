+++
title = "Get Information About a Disk Image"
description = "Use qemu-img to display disk image information"
+++

```bash
qemu-img info Win2k.img
```

This command extracts and displays relevant information about a disk image in plain text, including file format, disk size (actual and virtual), etc.

<kbd>qemu-img</kbd>  
    calls the qemu program to work with images
    
<kbd>info</kbd>  
    instructs the program to retrieve and display metadata
    
<kbd>Win2k.img</kbd>  
    path, name and extension of the input disk image 

-------

Example output:

```bash
$ qemu-img info Win2k.img 
image: Win2k.img
file format: qcow2
virtual size: 30 GiB (32212254720 bytes)
disk size: 914 MiB
cluster_size: 65536
Format specific information:
    compat: 1.1
    lazy refcounts: false
    refcount bits: 16
    corrupt: false
```

<kbd>file format</kbd>  
Supported formats include: 
```
blkdebug blklogwrites blkreplay blkverify bochs cloop 
copy-on-read dmg file ftp ftps host_cdrom host_device 
http https iscsi iser luks nbd null-aio null-co nvme 
parallels qcow qcow2 qed quorum raw rbd replication 
sheepdog ssh throttle vdi vhdx vmdk vpc vvfat
```

<kbd>virtual size</kbd>  
Many virtual disk image formats support specifying a **maximum** possible storage size, so that, for example, an emulated guest operating system will report seeing a larger disk without actually taking up that amount of space on the host operating system. In this example, the disk image could potentially hold up to 30 GiB (and will appear as a 30 GiB hard drive to an emulated OS), but only takes up the amount listed under "disk size".

<kbd>disk size</kbd>  
The actual amount of storage space taken by the disk image on the host operating system. In this example, only 914 MiB of the maximum 30 GiB have been used.

<kbd>cluster_size</kbd>  
Unique to virtual disk image formats like qcow2. Smaller cluster sizes can improve the image files size while larger cluster sizes generally provide better performance.
