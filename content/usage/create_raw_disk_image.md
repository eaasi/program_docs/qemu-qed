+++
title = "Create a Raw Disk Image"
description = "Use qemu-img to create a raw disk image"
+++

```bash
qemu-img create Win2k.img 20G
```

<kbd>qemu-img</kbd>  
    calls the QEMU program to work with images
    
<kbd>create</kbd>  
    instructs the program to produce a new image; new images will be "raw" by default
    
<kbd>Win2k.img</kbd>  
    defines the path and file name to store the image
    
<kbd>20G</kbd>  
    defines the size of the image as 20 gigabytes. K, M, G, and T can be used to create kilo-, mega-, giga-, or terabyte size disk images. 

A "raw" disk image does not support "copy-on-write" or other advantages of virtual disk image formats. The raw disk image will take up a full 20G of storage **on the host machine**, even if there is not 20G of meaningful data in the image.
