+++
title = "Using the QEMU Monitor"
description = "Send commands to running virtual machines using the QEMU Monitor"
+++

The QEMU Monitor is a terminal-like interface that allows the user to make certain adjustments or get information about an emulated guest system *while it's running* (in other words, the user does not have to completely stop the emulated guest and edit the command they used to originally launch their QEMU session).

This is particularly useful for:
- changing the disk image loaded into removable media drives, like CD-ROM or floppy drives
- send certain keyboard commands to the guest system that the user's host operating system might otherwise intercept (e.g. "ctrl-alt-delete" on Windows)
- freezing/unfreezing the running state of the VM (allowing the user to save and restore the precise condition of a running emulation, rather than booting the guest operating system from scratch)

### Opening the QEMU Monitor

By default, when launching a QEMU emulator, the user can switch from viewing the video output/screen of the emulated guest system to the QEMU Monitor by pressing <kbd>Ctl+Alt+2</kbd>.

![](qemu_monitor.png)

Pressing <kbd>Ctl+Alt+1</kbd> should return the user to the emulated guest.

Alternatively, users can also append the <kbd>-monitor stdio</kbd> flag to their QEMU command when they launch an emulation; this will redirect the QEMU Monitor to the command-line window that they used to call QEMU in the first place. This approach has the benefit of allowing the user to simultaneously view the screen of the emulated guest while passing QEMU Monitor commands. For instance:

    $ qemu-system-i386 -monitor stdio

results in:

![](monitor_stdio.png)

As the Monitor interface instructs, the user can type <kbd>help</kbd> into the QEMU Monitor to see a [complete](https://www.qemu.org/docs/master/system/monitor.html) list of commands and usage options in the QEMU Monitor. There's a lot available! Below are a few recommendations for getting started and/or convenient workflows that require use of the QEMU Monitor.

### Get Information About the Emulated System

The <kbd>info</kbd> Monitor command can be used to gather useful information about the running emulation and its current state.

<kbd>info version</kbd>  
  Shows the version of QEMU running.

<kbd>info block</kbd>  
  Shows information about "block" (storage/media) devices, such as CD-ROM, floppy, or hard disk drives, attached to your guest system (including the disk image file(s) currently loaded in them, if any); note this may include both virtual (emulated) devices AND potentially real hardware, such as a USB CD-ROM drive attached to your host system.

<kbd>info usb</kbd>  
  Shows any emulated USB devices attached to the *guest* system.

<kbd>info usbhost</kbd>  
  Shows any USB devices attached to the *host* system.

<kbd>info roms</kbd>  
  Shows information about the ROM file(s) in use by the selected emulator.

<kbd>info network</kbd>  
  Shows information about the emulated guest's network connection (if any has been defined).

<kbd>info history</kbd>  
  Shows the history of QEMU Monitor commands you have previously entered.

<kbd>info snapshots</kbd>  
  Shows the tag and other related information for any previously-saved snapshots of the running VM (see below)

### Swapping Removable Disks

The <kbd>change</kbd> QEMU Monitor command can be used to live-swap the disk image attached to an emulated removable media drive - allowing you to switch between e.g. floppy or CD-ROM disk images while the emulated computer is running (just like with the real deal!)

With the QEMU Monitor open (see above), first use the <kbd>info block</kbd> command to confirm the removable media drives available to your emulated system and, crucially, their names/labels. You will need this information to specify to the <kbd>change</kbd> command *which* drive you are trying to insert or change the disk image.

**By default**, QEMU will usually give your emulated computer one CD-ROM drive, labelled "ide1-cd0", and one floppy drive, labelled "floppy0". You are probably safe using these values if you aren't sure how to interpret the <kbd>info block</kbd> output; but things might get more complicated if you started your QEMU session with more than one CD-ROM or floppy drive attached.

Once you have identified the emulated drive in question, you can change the disk image in that drive (either inserting one if there is none there already, or swapping if there is already an image attached) by entering:

    (qemu) change [drive-label] [/host/path/to/file.iso]

Where:

<kbd>[drive-label]</kbd>  
    You should substitute with the value obtained in the previous step (e.g. "ide1-cd0" or "floppy0")

<kbd>[/host/path/to/file.iso]</kbd>  
    You should substitute with a file path (using the convention appropriate for your host operating system) to the disk image you would like to load into the emulated drive.  
    Note that the QEMU Monitor will accept either absolute file paths *or* file paths relative to the directory you were in when you originally called QEMU; but it will *not* tab/auto-complete directory or file path names for you. (It will also give you no help to properly escape spaces in file paths; it's highly recommended to avoid naming disk images with spaces in the file name or putting them in directories with spaces in their name!)

So, a successful <kbd>change</kbd> command will look something like:

    (qemu) change ide1-cd0 /home/user/Downloads/MSOffice97.iso

(If you'd like to *empty* an emulated removable media drive, rather than inserting or swapping in a new disk image, you can easily do this by running the <kbd>eject</kbd> QEMU Monitor command, e.g. <kbd>eject ide1-cd0</kbd>)

### Sending Key Combos

The <kbd>sendkey</kbd> QEMU Monitor command can be used to manually specify a keystroke or combination of keystrokes to send directly to the emulated guest system, without actually/literally pressing those keys on their physical keyboard. This can be critical if and when the QEMU user's host operating system intercepts certain keystrokes the user makes on their physical keyboard, e.g. Function buttons, the Escape button, or "Control-Alt-Delete". It may also help with debugging issues (e.g. determining whether a sticky/missing key is an issue with the host or the emulated input hardware, etc.)

For instance:

    (qemu) sendkey esc

Note that the value to plug in to the <kbd>sendkey</kbd> command can either be the *name* of the key (which may be an informal name/label like "esc", "ctrl", "cmd", etc.) or the raw hexadecimal value (e.g. the standard QWERTY keyboard hex values [here](https://www.rhydolabz.com/documents/Hex%20Table%20for%20key%20board%20decoder.pdf)).

To send several keystrokes simultaneously, use "-" to separate the key values, e.g.

    (qemu) sendkey ctrl-alt-f2

### Create a "Snapshot" of the Running VM

A "snapshot" in the context of the QEMU Monitor refers to a completely frozen running state of your VM, including running CPU processes, RAM, user data, etc. This is kind of like the "checkpoint" or "saved state" feature of video games and video game emulators: by creating and loading VM snapshots, you can jump right back into a running program instead of booting your emulated computer from scratch every time you start a QEMU session.

You can *only* create snapshots if your VM is using the QCOW2 disk image format as its (first) virtual hard drive. (See [Create a QCOW2 Disk Image](create_qcow2_disk_image) and [Booting from a Hard Disk Image](../booting_hard_disk))

To create a snapshot, use the QEMU Monitor <kbd>savevm</kbd> command:

    (qemu) savevm [tag]

Where <kbd>[tag]</kbd> is an arbitrary value you should fill with whatever descriptive tag/label/name you would like.

For instance, <kbd>savevm desktop</kbd> might be a good, quick descriptive name for a snapshot that jumps right to e.g. a Windows XP desktop instead of waiting for the entire XP boot process to complete.

To load/jump to a snapshot, use the QEMU Monitor <kbd>loadvm</kbd> command:

    (qemu) loadvm [tag]

If you would like a list of the available VM snapshots stored inside a QCOW2 file and their equivalent tag, use the <kbd>info snapshots</kbd> QEMU Monitor command once the VM is running.

#### A Warning About "Snapshots"
PLEASE NOTE that the snapshots created and loaded by the QEMU Monitor are essentially completely unrelated from the "Snapshot mode" enabled if you append a <kbd>-snapshot</kbd> flag to the command you use to invoke QEMU in the first place. In the latter context, "Snapshot mode" refers to running your emulation "read-only", with any and all changes to a disk that you make in emulation written to a temporary file (that is then ultimately discarded when you complete your session) instead of to the QCOW2 or whatever other disk image you are using to boot your session.