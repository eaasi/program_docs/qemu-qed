+++
title = "Create a QCOW2 Disk Image"
description = "Use qemu-img to create a qcow2 disk image"
+++

```bash
qemu-img create -f qcow2 Win2k.img 20G
```

<kbd>qemu-img</kbd>  
    calls the QEMU program to work with images
    
<kbd>create</kbd>  
    instructs the program to produce a new image
    
<kbd>-f qcow2</kbd>  
  specifies the output disk image format at "qcow2", a format native to the QEMU project (it is the second version, and latest/recommended version, of the format)
    
<kbd>Win2k.img</kbd>  
    defines the path and file name to store the image
    
<kbd>20G</kbd>  
    defines the size of the image as 20 gigabytes. K, M, G, and T can be used to create kilo-, mega-, giga-, or terabyte size disk images. 

Using the native QCOW2 disk image format with QEMU has several advantages:

  - the disk image will only take up space on the host machine depending on how much meaningful data is actually stored on/in it
  - the "copy-on-write" functionality allows QEMU to take advantage of *backing files*
  
If the user creates a new QCOW2 disk image from a specified **backing file**, then runs an emulated machine from the derivative QCOW2 image, QEMU will refer back to the original "base" file to run but only write changes to the derivative, leaving the base copy unchanged (and the derivative QCOW2 image potentially very small).

```bash
qemu-img create -f qcow2 -o backing_file=Input_Win2k.img \
Output_Win2k.qcow2
```

<kbd>-o backing_file=Input_Win2k.img</kbd>  
  specifies the "base copy" (path, name, and extension) for the output derivative QCOW2 image
  
<kbd>Output_Win2k.qcow2</kbd>  
  path, name, and extension for the new output disk image (QEMU is **extension-agnostic**; QCOW2-format files can use .img, .dd, .image, or any other file extension)
  
##### Notes on Using Backing Files

1. Moving, deleting or altering the base copy **will** corrupt any derivative copy-on-write/QCOW2 image based on it!!  
<br>
Copy-on-write images are wonderful for testing and disposable work. If keeping derivative copy-on-write files, it is highly recommended to set the original base copy image to read-only.

2. Copy-on-write files can be chained off of each other - that is, ```-o backing_file=``` can specify a QCOW2 file that is itself a QCOW2 derivative of another image.  

Use ```qemu-img info --backing-chain input_image.qcow2``` to see details of all disk images in a backing chain.
