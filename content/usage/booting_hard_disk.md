+++
title = "Booting from a Hard Disk Image"
description = "Boot an emulated machine from a hard disk image"
+++
```bash
qemu-system-i386 -drive file=filename.img
```

<kbd>qemu-system-i386</kbd>  
    calls the program and emulates an i386-architecture machine  
    
<kbd>-drive</kbd>  
    tells the program you are sending it a file mimicking a hard disk drive  
    
<kbd>file=filename.img</kbd>  
    path and name of the input file; QEMU is file extension-agnostic, so this could be a .dd, .qcow, .dmg, etc.
