---
title : "Windows 98"
tags: ["Windows", "i386"]
---

```bash
qemu-system-i386 -drive file=[Win98.img] \
- fda [Win98BootFloppy.img] -cdrom [Win98Install.iso] \ 
-m 512 -soundhw sb16 -vga cirrus -net nic,model=pcnet \
-boot a
```

<kbd>qemu-system-i386</kbd>  
      calls the program and specifies the i386 system architecture  
      
<kbd>-drive file=[Win98.img]</kbd>  
    loads the specified file as a hard drive (ideally, a blank disk image)
    
<kbd>-fda [Win98BootFloppy.img]</kbd>  
    loads the specified file (a bootable Windows 98 installation floppy disk image) into the computer's first floppy drive ("A:" drive); most Windows 9x (95, 98, and ME) operating systems had installers distributed on CD-ROMs that were not bootable, so you needed a dedicated "boot floppy" if starting an installation from scratch (if you find an installation CD-ROM that *is* bootable, skip this flag)
    
<kbd>-cdrom [Win98Install.iso]</kbd>  
    loads the specified file (a Windows 98 installation CD-ROM) into the CD-ROM drive  
    
<kbd>-m 128</kbd>  
    gives the emulated computer 128 MB of RAM  
    
<kbd>-soundhw sb16</kbd>  
    gives the emulated computer a SoundBlaster 16 sound card  
    
<kbd>-vga cirrus</kbd>  
    gives the emulated computer a Cirrus Logic video card  
    
<kbd>-net nic,model=pcnet</kbd>  
    gives the emulated computer an AMD PCNET ethernet adapter  
    
<kbd>-boot a</kbd>  
    directs the emulated computer to boot from the floppy ("A:") drive rather than the hard disk drive for installation; remove or switch this flag to <kbd>-boot c</kbd> after successful installation
