---
title: "Mac OS 9.2.1"
tags: ["MacOS", "ppc"]
---

```bash
qemu-system-ppc -M mac99 -m 512 \
-netdev user,id=network0 -device sungem,netdev=network0 \
-prom-env "boot-args=-v" -prom-env "auto-boot?=true" \
-prom-env "vga-ndrv?=true" -usb -device usb-mouse \
-device usb-kbd -hda [MacOS921.img] \
-cdrom [OS921_installer.iso] -boot d
```

<kbd>qemu-system-ppc</kbd>  
  calls the program and specific the PowerPC architecture
  
<kbd>-M mac99</kbd>  
  specifies that QEMU should specifically emulate a "Mac99"-based PowerMac machine/motherboard; this is essential to running Mac OS systems rather than other PowerPC-compatible operating systems
  
<kbd>-m 512</kbd>  
  gives the emulated computer 512 MB of RAM
  
<kbd>-netdev user,id=network0</kbd>  
  creates a virtual "user" network and assigns it the name "network0"; this automatically sets up some basic networking configuration and attaches the MacOS virtual machine to your host's LAN, recommended because it wil basically give you internet access out-of-the-box
  
<kbd>-device sungem,netdev=network0</kbd>  
  gives the emulated computer a SunGEM network adapter and attaches it to the virtual network specified above; this is the necessary *hardware* component to connect to the internet, host LAN, etc.
  
<kbd>-prom-env "boot-args=-v"</kbd>  
  sets a BIOS variable to display more information than usual when attempting to boot Mac OS; useful for troubleshooting problems
  
<kbd>-prom-env "auto-boot?=true"</kbd>  
  sets a BIOS variable to immediately/automatically attempt to boot Mac OS from the drive specified by <kbd>-boot</kbd>; useful/necessary if you don't want to learn OpenBIOS commands (you don't)
  
<kbd>-prom-env "vga-ndrv?=true"</kbd>  
  sets a BIOS variable that improves graphics quality
  
<kbd>-usb</kbd>  
  gives the emulated computer a USB bus/USB ports
  
<kbd>-device usb-mouse -device usb-kbd</kbd>  
  gives the emulated computer a USB mouse and a USB keyboard for user input, attached to the USB bus; useful for better input
  
<kbd>-hda [MacOS921.img]</kbd>  
  loads the specified file as a hard drive (ideally, a blank disk image)
  
<kbd>-cdrom [OS921_installer.iso]</kbd>  
  loads the specified file (a bootable MacOS 9.2.1 installation CD-ROM) into the CD-ROM drive
  
<kbd>-boot d</kbd>  
  directs the emulated computer to boot from the CD-ROM drive rather than the hard disk drive for installation; switch to <kbd>-boot c</kbd> after successful install
