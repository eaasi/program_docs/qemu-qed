---
title : "Windows XP (32-bit)"
tags: ["Windows", "i386"]
---
```bash
qemu-system-i386 -drive file=WinXP.img \
-cdrom WinXPInstall.iso -m 512 -soundhw ac97 \ 
-vga cirrus -net nic,model=rtl8139 -enable-kvm -boot d
```

<kbd>qemu-system-i386</kbd>  
  calls the program and specifies the i386 system architecture  
  
<kbd>-drive file=WinXP.img</kbd>  
  loads the specified file as a hard drive (ideally, a blank disk image)  
  
<kbd>-cdrom WinXPInstall.iso</kbd>
  loads the specified file (a bootable Windows XP installation CD-ROM) into the CD-ROM drive  
  
<kbd>-m 512</kbd>  
  gives the emulated computer 512 MB of RAM  
  
<kbd>-soundhw ac97</kbd>  
  gives the emulated computer an Intel AC'97 compatible sound card  
  
<kbd>-vga cirrus</kbd>  
  gives the emulated computer a Cirrus Logic video card  
  
<kbd>-net nic,model=rtl8139</kbd>  
  gives the emulated computer a RealTek RTL8139 ethernet adapter  
  
<kbd>-enable-kvm</kbd>  
  allows QEMU to use KVM virtualization, emulated machine will run more efficiently (will likely require admin privileges on the host computer)  
  
<kbd>-boot d</kbd>  
  directs the emulated computer to boot from the CD-ROM drive rather than the hard disk drive for installation; remove or switch to <kbd>-boot c</kbd> after successful install
