---
title : "Ubuntu 18.04 LTS (64-bit)"
tags: ["Linux", "x86-64"]
---
```bash
qemu-system-x86_64 -drive file=Ubuntu1804.img \
-cdrom Ubuntu1804_amd64.iso -m 2048 -soundhw ac97 \ 
-vga cirrus -net nic,model=e1000 -usb \
-device usb-tablet -enable-kvm -boot d
```

<kbd>qemu-system-x86_64</kbd>  
  calls the program and specifies the AMD64 (x86-64) system architecture  
  
<kbd>-drive file=[Ubuntu1804].img</kbd>  
  loads the specified file as a hard drive (ideally, a blank disk image)  
  
<kbd>-cdrom [Ubuntu1804_amd64].iso</kbd>
  loads the specified file (a bootable Ubuntu 18.04 installation CD-ROM) into the CD-ROM drive; take care to download the 64-bit/AMD64 version of the installation ISO  
  
<kbd>-m 2048</kbd>  
  gives the emulated computer 2048 MB of RAM  
  
<kbd>-soundhw ac97</kbd>  
  gives the emulated computer an Intel AC'97 compatible sound card  
  
<kbd>-vga cirrus</kbd>  
  gives the emulated computer a Cirrus Logic video card  
  
<kbd>-net nic,model=e1000</kbd>  
  gives the emulated computer an Intel Gigabit E1000 ethernet network device  
  
<kbd>-usb -device usb-tablet</kbd>  
  enables the USB bus on the emulated computer and attaches a USB tablet pointing device (absolute pointing devices usually allow for smoother/better user input than USB or PS/2 mice)  
  
<kbd>-enable-kvm</kbd>  
  allows QEMU to use KVM virtualization, emulated machine will run more efficiently (will likely require admin privileges on the host computer)  
  
<kbd>-boot d</kbd>  
  directs the emulated computer to boot from the CD-ROM drive rather than the hard disk drive for installation; remove or switch to <kbd>-boot c</kbd> after successful install
