---
title : "FreeDOS 1.2"
tags: ["Windows", "i386"]
---

[FreeDOS](https://freedos.org/) is an open source re-implementation of the MS-DOS operating system, allowing DOS-compatible software (including early versions of Windows!) to be run on modern machines.

Many online tutorials for installing FreeDOS recommend creating a ~200 MB disk image for the system drive for your FreeDOS virtual machine, but that is not actually enough space to fit all of the packages available on the standard FreeDOS installer. QEMU QED recommends a disk image approximately 2 GB.

The latest stable version of FreeDOS as of this writing is 1.2, but these recommended QEMU settings should remain compatible with future versions.

```bash
qemu-system-i386 -drive file=FreeDOS.img -m 16 \
-soundhw sb16 -vga cirrus -net nic,model=pcnet \
-cdrom FreeDOS_installer.iso
```

<kbd>qemu-system-i386</kbd>  
      calls the program and specifies the i386 system architecture  
      
<kbd>-drive file=[FreeDOS.img]</kbd>  
    loads the specified file as a hard drive (ideally, a blank disk image)
    
<kbd>-m 16</kbd>  
    gives the emulated computer 16 MB of RAM  
    
<kbd>-soundhw sb16</kbd>  
    gives the emulated computer a SoundBlaster 16 sound card  
    
<kbd>-vga cirrus</kbd>  
    gives the emulated computer a Cirrus Logic video card  
    
<kbd>-net nic,model=pcnet</kbd>  
    gives the emulated computer an AMD PCNET ethernet adapter  
    
<kbd>-cdrom [FreeDOS_installer.iso]</kbd>  
    loads the specified file (a FreeDOS installation CD-ROM) into the CD-ROM drive; this flag can be removed after installation is complete
