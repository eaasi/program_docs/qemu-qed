---
title : "Yellow Dog Linux 2.0"
tags: ["Linux", "ppc"]
---

[Yellow Dog Linux](https://en.wikipedia.org/wiki/Yellow_Dog_Linux) was the only third-party operating system to be officially licensed for distribution on Apple (PowerPC) hardware.

- [Installation](#installation)
- [Post-Installation](#post-installation)

### Installation

Yellow Dog installation media will not pick up on a completely blank/unformatted hard drive. The disk image **must** be formatted with the HFS+ file system before you can successfully run the Yellow Dog installation program.

This can be accomplished using contemporary MacOS installation media, e.g. a bootable installation disc for MacOS 9.2.1, or similar.

Once a blank disk image has been created to install Yellow Dog Linux on, start the installation process with:

```bash
qemu-system-ppc -M mac99 -m 1024 \
-drive file=YellowDog.img \
-cdrom MacOS9_installer.iso -boot d
```

<kbd>qemu-system-ppc</kbd>
  calls the program and specifies the PowerPC system architecture
  
<kbd>-M mac99M</kbd>
  specifies that QEMU should specifically emulate a "Mac99"-based PowerMac machine/motherboard
  
<kbd>-m 1024</kbd>
  gives the emulated computer 1024 MB of RAM
  
<kbd>-drive file=YellowDog.img</kbd>  
  loads the specified file as a hard drive (ideally, a blank disk image)
  
<kbd>-cdrom MacOS9_installer.iso</kbd>
  loads the specified file (a bootable MacOS 9 installation CD-ROM) into the CD-ROM drive    
  
<kbd>-boot d</kbd>  
  directs the emulated computer to boot from the CD-ROM drive rather than the hard disk drive for installation; remove or switch to <kbd>-boot c</kbd> after successful install
  
Once the emulated machine has booted into the MacOS 9 installation disc, navigate to the "Utilities" menu, then select "Drive Setup". Use this program to initialize (format) the available, blank ATA disk (this is the "YellowDog.img" blank hard disk image).

Once the disk has been initialized, quit out of Drive Setup and Shut Down the machine. Then, relaunch QEMU with the following, adjusted command:

```bash
qemu-system-ppc -M mac99 -m 1024 \
-drive file=YellowDog.img \
-net nic,model=rtl8139 -soundhw ac97 \ 
-g 1024x768x8 -cdrom YellowDogInstaller.iso -boot d
```

<kbd>-net nic,model=rtl8139</kbd>  
  gives the emulated computer a RealTek RTL8139 ethernet adapter  

<kbd>-soundhw ac97</kbd>  
  gives the emulated computer an Intel AC'97 compatible sound card
  
<kbd>-g 1024x768x8</kbd>
  manually sets the emulated machine's monitor grahics to to 1024x768 resolution and 8-bit color; this flag **must** be set during installation or else QEMU will not render the installer program properly, which makes it impossible to navigate
  
<kbd>-cdrom YellowDog_installer.iso</kbd>
  loads the specified file (a bootable Yellow Dog Linux installation CD-ROM) into the CD-ROM drive
  
<kbd>-boot d</kbd>  
  directs the emulated computer to boot from the CD-ROM drive rather than the hard disk drive for installation

The graphics-based (X server) Yellow Dog installer does not work properly with QEMU. It is highly recommended to use the text-based installer instead to navigate the following process.

It is also recommended to select a "Custom" installation to make the following tweaks, which may not run correctly during an auto/default installation:
  - Select an ADB mouse (just during installation, this can be adjusted after the install)
  - Drive partitioning:
    - delete all/any Macintosh/HFS partitions and set up a boot, swap, and system partition
    - 10 MB for the bootloader partition, 128 MB for Linux swap space, and max out the rest of your available storage with a Linux regular partition
    - accept all auto-configured mount points for swap and system partitions
  - accept default options for starting system services on boot
  - network configuration will not work during installation; skip this and set up network options after successful install
  - install yaboot to the bootloader partition
  - for your monitor select Custom settings:
    - 1024x768 @ 75Hz
    - monitor is capable of all resolutions, 32-bit color
    - graphical login screen

When installation is complete, shut down the QEMU machine.

### Post-Installation

Following successful installation, boot your Yellow Dog Linux emulated machine with the following, adjusted QEMU command:

```bash
qemu-system-ppc -M mac99 \
-prom-env "boot-device=hd:,\yaboot" \
-prom-env "boot-args=conf=hd:,\yaboot.conf" \
-drive file=YellowDog.img -m 1024 -net nic,model=rtl8139 \
-soundhw ac97 -usb -device usb-tablet
```

<kbd>-prom-env "boot-device=hd:,\yaboot" -prom-env "boot-args=conf=hd:,\yaboot.conf"</kbd>
  explicitly directs the QEMU PowerPC BIOS (OpenBIOS) to the correct location for Yellow Dog Linux's "yaboot" bootloader; otherwise, OpenBIOS will not properly find yaboot automatically and Yellow Dog Linux will not boot
  
<kbd>-usb -device usb-device</kbd>
  gives the emulated machine a USB bus and attaches a USB tablet pointing device; this will make user/mouse input more precise
