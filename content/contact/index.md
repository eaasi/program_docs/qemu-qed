---
title           : "Contact"
description     : "See how to contact me."
sitemapExclude  : true
---

![A white man with brown hair and glasses smiling and standing with his arms crossed.](profile_pic.png)

QEMU QED is currently maintained by Ethan Gates, Software Preservation Analyst at Yale University Library and User Support Lead for the EaaSI program of work.

You can also find me at:

{{< social >}}
