+++
title = "QEMU QED"
disableTitleSeparator = true
+++

#### The Missing Manual for Digital Preservation

QEMU (Quick EMUlator) is a powerful and flexible open-source command line application for computer emulation and virtualization. It is potentially of great use to archivists, conservators, and other digital preservation practitioners in need of recreating legacy computing systems, whether to interact with or migrate software-dependent data. But it is also a complex application with a vast range of configuration options. QEMU QED is intended to lower the barrier to first-time users by breaking down QEMU use into more digestible concepts and recipes.

QEMU QED is a work-in-progress. For full QEMU docs, check out the project's official [website](https://qemu.org), or our linked [resources](https://eaasi.gitlab.io/program_docs/qemu-qed/resources).

-----------

##### Using the Site

The main QEMU QED knowledge base is in the [Using QEMU](/usage/) section of the site. You can browse or search these posts to find guidance for performing certain actions or exploring QEMU's capabilities!

Other pages provide instructions for [installing QEMU](https://eaasi.gitlab.io/program_docs/qemu-qed/installing/), recommended command-line [recipes](https://eaasi.gitlab.io/program_docs/qemu-qed/recipes/) for emulating certain operating systems in QEMU, a [glossary](https://eaasi.gitlab.io/program_docs/qemu-qed/glossary/) of relevant computing terms, and [more resources](https://eaasi.gitlab.io/program_docs/qemu-qed/resources/) for exploring the wide world of QEMU.

You can also switch this web site between dark and light mode and pick an accent color of your choosing using the color picker in the footer!

-----------

##### Credits and Contributing

QEMU QED began as a project during the iPRES 2019 conference, as part of "Reading the Matrix: A Hackathon Linking Digital Forensics to User Access", sponsored by the teams behind [EaaSI](https://www.softwarepreservationnetwork.org/emulation-as-a-service-infrastructure) and [BitCurator](https://bitcuratorconsortium.org).

The original site was based on [ffmprovisr](https://amiaopensource.github.io/ffmprovisr) with code and content contributions from Ashley Blewer and Nick Krabbenhoeft. It was redesigned, migrated, and expanded in Hugo in summer 2021.

Please contribute to QEMU QED through our [GitLab](https://gitlab.com/eaasi/program_docs/qemu-qed) page or by [getting in touch](https://eaasi.gitlab.io/program_docs/qemu-qed/contact) with suggestions directly!
