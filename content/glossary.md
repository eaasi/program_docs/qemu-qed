---
title : "Glossary"
---

##### Architecture
  (**aka:** System Architecture, CPU architecture, Instruction Set Architecture, Instruction Set)
  
  A computer's "architecture" is an abstract model of its operations. The computer's architecture determines many of its fundamental features, including compatible hardware and software. In practical terms, architectures are *implemented* via the [processor](#cpu).
  
  In emulation and virtualization it is often not necessary to emulate a *specific* model of processor - mimicking a *generic* processor that implements a particular archiecture is often good enough to run software and emulate other hardware compatible with that architecture.
  
##### Boot/Booting

  To start up a computer. "Bootable" software is necessary to run and use a computer in any meaningful way.

##### CPU
  (**aka:** Central Processing Unit, Central Processor, Main Processor, Processor)
  
  A piece of electronic circuitry that executes computer programs. It is frequently compared to the "brain" of a computer: it processes *everything* run on your computer, from basic instructions to complex functions.
  
  Emulators can recreate specific makes and models of CPU, or "generic" CPUs that conform to a desired [architecture](#architecture).

##### Disk Image

  A software copy of a physical disk. A disk image recreates all of the data on a disk, including file structure, and all files and folders from the disk, into a single (usually) file. Disk images can be created from existing hard disks, solid state drives, and optical media, or arbitrarily (*entirely* in software).  
  
  For the data in a disk image to be readable by a computer, the image must usually first be *mounted* by a compatible [operating system](#operating-system) or disk utility program.

##### Emulation

  The process by which one computer system imitates or reproduces another computer system. Most emulation is done via software, using emulators like QEMU.
  
  The term "full-system emulation" may be seen in some contexts, particularly in contrast with [virtualization](#virtualization). This generally means that an *entire* [guest](#guest) computer ([CPU](#cpu), [memory](#ram), all peripheral devices, etc.) is recreated by the emulator, rather than piggy-backing on some or any pieces of real hardware from the user's [host](#host) system. For instance, full-system emulation is essential to recreate a video game console on a laptop, where absolutely no hardware components could possibly be shared. Emulating older PC systems, on the other hand, may be accomplished with a blurrier combination of emulation and virtualization.

##### Guest

  In virtual machines and emulated environments, the "guest" system and/or operating system is the one run *inside* an emulator like QEMU.

##### Host

  In virtual machines and emulated environments, the "host" system or operating system is the one running the emulator. This is presumably the user's everyday, contemporary OS.

##### LAN
  (**aka:** Local Area Network)
  
  A computer network that connects a limited number of devices, usually within the same room or building (e.g. an office, school, or home). QEMU can recreate virtual LANs and/or connect VMs to your actual LAN as a way to connect guest systems to the internet, networked storage, networked printers, and so forth.

##### NIC
  (**aka:** Network Interface Controller, Network Interface Card, Network Adapter, LAN Adapter, Physical Network Interface)
  
  A piece of hardware that allows a computer to connect to a network. Extremely common examples would be Ethernet or Wi-Fi NICs. An emulated NIC is necessary to connect a QEMU VM to the internet or any other device on the host machine's [LAN](#LAN).

##### Operating System
  
  An operating system (OS) is software that allows the user to control hardware, manage other software, and provide other basic/common computing services. Frequently, the first step in emulation or creating a new VM via QEMU is to install an operating system.
  
  Operating systems are usually [bootable](#bootbooting), though the programs that *install* operating systems may or may not be bootable (particularly with systems from the '90s or earlier).

##### RAM
  (**aka:** Random-Access Memory, sometimes just shortened to "memory")
  
  A type of computer memory that can be read and changed in any order; as opposed to most storage devices (hard disk drives, optical media, magnetic tape, etc), RAM can be read or written to very quickly, which makes it ideal for temporarily storing/caching *active* data, like the program a user is currently running and the files that software is currently accessing.
  
  The more RAM a machine has, generally the better/faster it will perform from the user's perspective, including emulated/virtual systems. However, for various reasons, [operating systems](#operating-system), especially older ones, may have upper limits on how much RAM they can use.

##### Sound Card
  (**aka:** Audio Card)
  
  A hardware component that allows a computer to input and output audio signals, e.g. to the computer's speakers. As of the ~2010s, most computer systems integrate sound capability into the motherboard, making it unnecessary and redundant to emulate a seprate, dedicated sound card. However, many older systems will need an emulated sound card to create any/better sound.

##### Video Card
  (**aka:** Graphics Card, Display Card, Graphics Adapter, Display Adapter)
  
  A hardware component that allows a computer to input and output video signals, e.g. to a monitor. Since the dawn of personal computing, machines pretty much always have *some* kind of built-in graphics capability (even a command-line prompt needs to be seen on a monitor), but more advanced video options and graphics-based software often required a dedicated card. Likewise, virtual machines, particularly emulating older systems, may require an emulated video card.
  
  Modern, external GPUs are related and basically serve the same purpose (technically speaking, all video cards have a GPU), but are focused on specific intensive tasks like rendering 3D graphics, machine learning, cryptocurrency mining, etc. It will almost certainly become necessary for archivists to emulate GPUs in the future, and there is luckily already some motion [within the  QEMU community](https://virgil3d.github.io/) towards such options.

##### Virtualization

  Virtualization allows a single computer to run multiple operating systems or multiple computing environments. It is designed to allow a user to run multiple OSes (or to create isolated computing environments) on a computer that would otherwise be hardware-compatible with *either* operating system and its software.
  
  For instance, both Windows 10 and macOS 10.15 (Big Sur) can be run on x86_64-[architecture](#architecture) computers. Instead of choosing only to use one operating system, a user can install one and then run the other inside a [virtual machine](#virtual-machine) via virtualization.
  
  Virtualization is very similar to [emulation](#emulation) but more efficient because it does not require mimicking the *entire* computer - in virtualization, the guest and host OS are both compatible with the same processor, for instance, so there is no need to emulate a [CPU](#cpu). However, virtualization is susceptible to breaking over time as systems advance and older OSes become incompatible with current hardware. So emulation is a safer strategy for *long-term* storage and running of virtual machines or computing environments.
  
  QEMU is capable of both emulation *and* virtualization, so both strategies can be used together, and adjusted depending on the desired [guest](#guest) system in question.

##### Virtual Machine

  The virtualization or emulation of a computer system. Virtual machines (VMs) were [defined](https://dl.acm.org/doi/10.1145/361011.361073) early on as "an efficient, isolated duplicate of a real computer machine," so they are intended to contrast with "real", physical hardware. VMs are used to run [guest](#guest) systems; though they may communicate and make use of real hardware attached to their [host](#host).
