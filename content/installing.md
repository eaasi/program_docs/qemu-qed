+++
title = "Installing QEMU"
description = "Instructions for installing QEMU on various systems"
aliases = ["install"]
+++

The following are guidelines for installing QEMU on different operating systems.

QEMU QED recommends using the most up-to-date version of QEMU available for your OS, but may specifically recommend earlier versions based on compatibility with emulating certain legacy operating systems. Any example commands used will note when if they are only recommended for a particular QEMU version.

- [BitCurator/Ubuntu/Linux](#bitcuratorubuntulinux)
- [MacOS](#macos)
- [Windows](#windows)
  - [Windows Subsystem for Linux](#windows-subsystem-for-linux)
  -------
- [Running in a Virtual Machine or WSL](#running-in-a-virtual-machine-or-wsl)
- [Versioning](#versioning)

## BitCurator/Ubuntu/Linux
QEMU can be easily installed on any Debian/Ubuntu-based system, such as BitCurator, using common package managers. However, be aware that the version installed may vary depending on the package manager used and/or distribution version (e.g. using `apt` on Ubuntu 20.04 will install a different version of QEMU than on Ubuntu 22.04).

Using **apt** , install *both* `qemu-system` and `qemu-utils` packages to make sure you get access to all of QEMU's various system emulators and utility programs like `qemu-img`:
```bash
apt install qemu-system qemu-utils
```
Meanwhile, **[Homebrew](https://docs.brew.sh/Homebrew-on-Linux)** on Linux can also be used to install all QEMU emulators and utilities like so:
```bash
brew install qemu
```

Please consult the [official docs](https://www.qemu.org/download/#linux) for package manager recommendations for other distributions.

## MacOS
QEMU is best installed on MacOS via [Homebrew](https://brew.sh).
```bash
brew install qemu
```

## Windows
The QEMU project does not officially maintain any Windows installers but recommends binaries [provided](https://qemu.weilnetz.de/w64/) by their contributor Stefan Weil.

Weil's installers are named by date rather than version. Consult the History notes to match installers to your desired version (or just use the latest).

Weil's binaries can also be installed using the [Chocolatey](https://chocolatey.org/) package manager:
```powershell
choco install qemu
```

### Windows Subsystem for Linux
WSL users should also be able to install QEMU in their WSL distribution using the Linux instructions above.

--------------------

# Running in a Virtual Machine or WSL
Proceed with some caution if installing QEMU in a virtual machine, compatibility layer (like Wine), or the Windows Subsystem for Linux! QEMU's emulation should still function fine in such environments, but certain features or functionality discussed elsewhere in QEMU QED - particularly virtualization of guest operating systems using KVM, or networking - may not work without additional setup, or at all.

---------------------

# Versioning
A brief note/explanation of QEMU's versioning. Since version 3.0.0, QEMU uses a fairly unique "time-based" version numbering scheme:

- **major**   
  automatically incremented by 1 for the first release *every year*
- **minor**  
  reset to 0 with each major increment (i.e. at the beginning of each calendar year), otherwise incremented by 1 for each release from QEMU's main git branch
- **micro**  
  always 0 for releases from QEMU's master git branch, incremented by 1 for each stable branch release
  
For instance: 6.0.0 was the first release of the year 2021, and 7.0.0 the first release of 2022. Most package managers will likely only concern themselves with the stability of "minor" point releases (6.1.0, 6.2.0, etc) and never pick up on "micro" ones.
