---
title : "More Resources"
---

- [Official QEMU documentation](https://qemu-project.gitlab.io/qemu/)
- [QEMU User Documentation Wiki](https://wiki.qemu.org/Category:User_documentation)
  - contains some information that may not have been merged into official docs
- [QEMU Wikibooks](https://en.wikibooks.org/wiki/QEMU)
  - not officially affiliated with QEMU project
- [QEMU Tutorials, Computer History Wiki](https://gunkies.org/wiki/Category:QEMU_Tutorials)
- [QEMU Advent Calendars](https://www.qemu-advent-calendar.org/2020/)
- Ethan's [emulation-resources](https://gitlab.com/EG-tech/emulation-resources) repository
- [Emulation-as-a-Service Infrastructure Training Modules](https://www.softwarepreservationnetwork.org/tag/training-module/)

----------
### Bibliography

- ["QEMU - A Crucial Building Block in Digital Preservation Strategies"](http://eprints.rclis.org/15406/1/qemu-in-ltp.pdf), Dirk von Suchodoletz, Klaus Rechert, Achille Nana Tchayep, 2011  
([Wayback](https://web.archive.org/web/20170809064338/http://eprints.rclis.org/15406/1/qemu-in-ltp.pdf))
- ["How to Party Like It's 1999: Emulation for Everyone"](https://journal.code4lib.org/articles/11386), Dianne Dietrich, Julia Kim, Morgan McKeehan, and Alison Rhonemus, *The Code4Lib Journal* Issue 32, 2016-04-25 ([Wayback](https://web.archive.org/web/20201125191958/https://journal.code4lib.org/articles/11386))
- ["_Floating Time_ with QEMU at the Denver Art Museum](http://www.dylanlorenz.net/files/Floating-Time-with-QEMU-at-the-DAM.pdf), Dylan Lorenz, *dylanlorenz.net*, 2019-09-24 ([Wayback](https://web.archive.org/web/20190924135128/http://www.dylanlorenz.net/files/Floating-Time-with-QEMU-at-the-DAM.pdf))
